<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>
<?php
        $pathToVideo="uhm.mp4";
        $video = Youtube::upload($pathToVideo, [
                'title'       => 'My Awesome Video',
                'description' => 'You can also specify your video description here.',
                'tags'        => ['foo', 'bar', 'baz'],
                'category_id' => 10
        ]);

        return $video->getVideoId();
        ?>
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Laravel 5</div>
            </div>
        </div>
    </body>
</html>
